// let Num = prompt("Give me a number: ")

// for (let numCount = 0; numCount<=Num; numCount--){
//     console.log(numCount)
// }

/* Activity:
1. In the S16 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable number that will store the value of the number provided by the user via the prompt.
4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
5. Create a condition that if the current value is less than or equal to 50, stop the loop.
6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7. Create another condition that if the current value is divisible by 5, print the number.
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.
13. Create a git repository named S16.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle. */



let number = prompt("Give me a Number: ");
console.log(`The number you provided is ${number}`);

for (let numCount = 0; number>=numCount; number--){
    // console.log("Number: " + number);
    if (number<=50){
        console.log(`The current number is at 50. Terminating the loop`)
        break;
    }
    else if (number%10 ===0){
        console.log(`The number is divisible by 10. This number will be skipped.`)
    }
    else if (number%5 ===0){
        console.log(`${number}`)
    }
}

let word = "supercalifragilisticexpialidocious";
let consonant = "";
let vowel="";



for (let i = 0; i < word.length; i++){
	if (
        word[i].toLowerCase() == "a" ||
        word[i].toLowerCase() == "e" ||
        word[i].toLowerCase() == "i" ||
        word[i].toLowerCase() == "o" ||
        word[i].toLowerCase() ==  "u"
		) {
		vowel+=word[i];
	}else{
		consonant+=word[i];
	}
}
console.log(word);
console.log(consonant);
